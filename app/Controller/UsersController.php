<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {
    public $helpers = array('Html', 'Form');

    public $components = array('Session');

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('No user found'));
        }

        if ($id != $this->user['id']) {
            throw new Exception(__('You are not allowed to view this page'));
        }

        $this->set('user', $this->user);
    }

    public function register() {
        if (!empty($this->user)) {
            $this->redirect(array('action' => 'view', $this->user['id']));
        }

        if ($this->request->isPost()) {

            if ($this->addUser($this->request['data']['User'])) {
                $this->Session->setFlash(__('Thank you for registering'));

                return $this->redirect(
                    array(
                        'action' => 'login'
                    )
                );
            }

            $this->Session->setFlash(__('There has been an error'));
        }
    }

    public function login() {
        if (!empty($this->user)) {
            $this->redirect(array('action' => 'view', $this->user['id']));
        }

        if ($this->request->isPost()) {

            $user = $this->User->find(
                'first',
                array(
                    'conditions' => array(
                        'User.username' => $this->request['data']['User']['username'],
                        'User.password' => md5($this->request['data']['User']['password'])
                    )
                )
            )['User'];

            if (!is_null($user)) {
                //Save user data to the session
                $this->Session->write('User.id', $user['id']);
                $this->Session->write('User.user_level', $user['user_level']);

                return $this->redirect(
                    array(
                        'action' => 'view',
                        $user['id']
                    )
                );
            }

            $this->Session->setFlash('invalid user/password');
        }
    }

    public function logout() {
        $this->Session->delete('User.id');
        $this->Session->setFlash(__('You have logged out'));

        $this->redirect('/');
    }

    private function addUser($userData) {
        if ($userData['password'] !== $userData['password_confirm']) {
            return false;
        }

        $userData['password'] = md5($userData['password']);

        $this->User->create();

        if ($this->User->save($userData)) {
            return true;
        }

        return false;
    }
}
