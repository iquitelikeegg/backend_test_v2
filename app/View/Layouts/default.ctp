<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());

$userLevel = null;

if (!is_null($this->Session->read('User.id'))) {
    $userLevel = (int) $this->Session->read('User.user_level');
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('main');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body <?php if ($userLevel === 1) echo 'class="front-end"'; ?>>
	<div id="container">
		<div id="header">
			<?php if ($userLevel === 1) : ?>
				<h1>Frontend test v1</h1>
			<?php else : ?>
				<h1>Backend test v1</h1>
			<?php endif; ?>
			<nav>
				<a href="/">home</a>
				<?php if (is_null($this->Session->read('User.id'))) : ?>
					<a href="/users/register">register</a>
					<a href="/users/login">login</a>
				<?php else: ?>
					<a href="/users/view/<?php echo $this->Session->read('User.id'); ?>">profile</a>
					<a href="/users/logout">logout</a>
				<?php endif; ?>
			</nav>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">

		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
