<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>

<h1>Welcome to the backend test!</h1>

<p>This is a simple exercise designed to test your backend skills, and your
	ability to navigate through a simple application to acheive a goal</p>

<h3>Tasks</h3>

<ul>
	<li>The username for this site is "admin". But you don't know the password!</li>
	<li>Please log in to the site using the "admin" username</li>
	<li>There are several ways to achieve this task and you have free access to
		the codebase and the database - so dive in!</li>
</ul>
