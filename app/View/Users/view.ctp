<h1>Hello, <?php echo $user['username']; ?></h1>

<p>Your permission level is: <strong><?php echo User::getUserLevel($user['user_level']); ?></strong></p>

<h3>Tasks</h3>

<ul>
    <?php if ($user['user_level'] == 0) : ?>
        <li>Well done! you are now logged in. </li>
        <li>Above is displayed what permission level your user has. This value
            should read <em>"user"</em></li>
        <li>Please gain "admin" permissions for this website, when you do the value
            above will display <em>"admin"</em></li>
        <li>You can do this either by creating a new user, or modifying the
            existing user.</li>
        <li>When you have done this, this text will change!</li>
    <?php else : ?>
        <li>You are now logged in as the admin! Well done!</li>
        <li>The final task will be a front end based task - to test your ability
            to manipulate css and html</li>
        <li>Please write some css to make the menu bar for this website resemble
            this image: <br /><br />
        <img width="100%" src="/img/backend-test-button-styles.png" /></li>
    <?php endif; ?>
</ul>
