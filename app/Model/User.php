<?php

class User extends AppModel {

    const LEVEL_USER  = 0;
    const LEVEL_ADMIN = 1;

    public $validate = array(
        'username' => array(
            'rule' => 'notEmpty'
        ),
        'password' => array(
            'rule' => 'notEmpty'
        )
    );

    public static function getUserLevel($userLevel) {
        $userLevels = array(
            self::LEVEL_USER  => 'user',
            self::LEVEL_ADMIN => 'admin'
        );

        return $userLevels[$userLevel];
    }
}
