The Backend Test

This is a simple test application, written in PHP, which is designed to test your
ability to navigate and dissect a simple application using the codebase and the database.

This test is written using the [CakePHP Framework](www.cakephp.org). Follows
the MVC architecture guidelines as implemented in CakePHP.

## To set up:

Now that you have cloned the repository - create a branch (git checkout -b sandbox master).

(Where "APP_ROOT" is the directory where you have checked out the repository)

Import the database located at APP_ROOT/_database

Set up the application on a local server (using localhost or a virtualhost if preferred)

Copy the file APP_ROOT/app/Config/database.php.default to APP_ROOT/app/Config/database.php
and add the username and password required to access your database.

Fire up your browser and go to the url where you've set up the test!
